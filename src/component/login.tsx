import React from 'react';
import { Link } from 'react-router-dom';
import { FormControl, Container, Row, Col, FormLabel, Button } from 'react-bootstrap';

export const Login = () => {
  return (
    <>
      <Container>
        <Row className="mt-4">
          <Col xs={6}>
            <FormLabel>Login</FormLabel>
            <FormControl size="sm" />
          </Col>
        </Row>
        <Row className="mt-2">
          <Col xs={6}>
            <FormLabel>Password</FormLabel>
            <FormControl size="sm" />
          </Col>
        </Row>
        <Row className="mt-2">
          <Col>
            <Button size="sm" variant="primary" onClick={() => {}}>
              Save data
            </Button>
          </Col>
        </Row>
        <Row className="mt-2">
          <Col>
            <Link to="/">Back to menu</Link>
          </Col>
        </Row>
      </Container>
    </>
  );
};
