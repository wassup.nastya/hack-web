import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { Login } from './component/login';
import { Home } from './component/home';
import { Main } from './component/main';

export const App = () => {
  return (
    <div className="App">
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/home" component={Home} />
        <Route path="/" component={Main} />
      </Switch>
    </div>
  );
};
