import { ActionsObservable, combineEpics } from "redux-observable";
import { from, throwError } from "rxjs";
import { catchError, filter, map, mergeMap } from "rxjs/operators";
import { isOfType } from 'typesafe-actions';

import { setData } from "./actions";
import { ActionType } from "./actionType";
import { getData } from './api';

const getEventsEpic = (action$: ActionsObservable<{ type: string, data: number }>) => action$.pipe(
  filter(isOfType(ActionType.GETDATAASYNC)),
  mergeMap(action =>
    from(getData(action.data)).pipe(
      map(response => setData(response.data.name)),
      catchError(error => throwError(error))
    )
  )
);

export const epic = combineEpics(getEventsEpic);