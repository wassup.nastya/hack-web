import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { getDataAsync } from 'data/actions';
import { StoreType } from 'core/rootReducer';

export const Home = () => {
  const dispatch = useDispatch();

  const name = useSelector((state: StoreType) => state.data.name);

  useEffect(() => {
    dispatch(getDataAsync(1));
  }, [dispatch]);

  return (
    <Container className="mt-4">
      <Row>
        <Col xs={6}>Home</Col>
      </Row>
      <Row className="mt-2">{name != null && <Col xs={6}>{`Data from server: ${name}`}</Col>}</Row>
      <Row className="mt-2">
        <Col xs={6}>
          <Link to="/">Back to menu</Link>
        </Col>
      </Row>
    </Container>
  );
};
