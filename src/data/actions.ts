import { ActionType } from './actionType';
import { Action } from './model';

export const getDataAsync: (data: number) => Action = (data) => {
  return { type: ActionType.GETDATAASYNC, data };
};

export const setData: (data: string) => Action = (data) => {
  return { type: ActionType.SETDATA, data };
};
