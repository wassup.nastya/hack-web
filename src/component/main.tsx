import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';

import { VerticalBar } from './verticalBar';

export const Main = () => {
  return (
    <Container className="mt-4">
      Hello! Please choose page
      <Row className="mt-1">
        <Col xs={6}>
          <Link to="/login">
            <span>Login</span>
          </Link>
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <VerticalBar />
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <Link to="/home">
            <span>Home</span>
          </Link>
        </Col>
      </Row>
    </Container>
  );
};
