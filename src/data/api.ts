import axios from 'axios';

const url = 'http://swapi.dev/api/planets/';

export const getData = (i: number) => axios.get(url + `${i}`);